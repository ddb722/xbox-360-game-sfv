# xbox 360 game SFV

Game files verification database for XBOX 360 (RGH/JTAG)

. This is for the **extracted game files**, so this is mainly for **(RGH/JTAG)**  
. Using simple CRC32, it is the fastest to calculate. And also:


> The probability of a corrupted file having the same checksum as its original is exceedingly small, unless deliberately constructed to maintain the checksum.



## Programs you can use

- [RekSFV](https://www.majorgeeks.com/files/details/reksfv.html) 
- [QuickSFV](https://www.quicksfv.org/)

I've been using RekSFV and it works fine.


## Contribute

**Name convention** : The filename of the SFV file should be the generic game name followed by `[TITLE ___] [MEDIA ___]`   
e.g. `Resident Evil Remastered [TITLE 43430841] [MEDIA 288576CA]`

- You can find the TITLE/MEDIA ID of a game from the Aurora Launcher, or by using any pc program that can parse this data *(e.g. JTAG Content Manager)*
- This is to differentiate the different versions of a game release *(PAL/US/etc)*
